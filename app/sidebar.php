<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php if(strpos($_SERVER['REQUEST_URI'],'berita') !== false){echo 'active ';}?>treeview">
          <a href="#"><i class="fa fa-newspaper-o"></i> <span>Berita</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
			
            <li <?php
			$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			if (strpos($url,'daftar_berita') !== false) {
				echo 'class="active"';
			}
			?>><a href="daftar_berita.php"><i class="fa fa-circle-o"></i> Daftar Berita</a></li>
            <li <?php
			$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			if (strpos($url,'buat_berita') !== false) {
				echo 'class="active"';
			}
			?>><a href="buat_berita.php"><i class="fa fa-circle-o"></i> Buat Berita</a></li>
			<li <?php
			$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			if (strpos($url,'verifikasi_berita') !== false) {
				echo 'class="active"';
			}
			?>><a href="verifikasi_berita.php"><i class="fa fa-circle-o"></i> Verifikasi Berita<small class="label pull-right bg-red">3</small></a></li>
          </ul>
        </li>
		<li class="<?php if(strpos($_SERVER['REQUEST_URI'],'pengumuman') !== false){echo 'active ';}?>treeview">
			<a href="#"><i class="fa fa-book"></i> <span>Pengumuman</span><i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li <?php
				$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
				if (strpos($url,'daftar_pengumuman') !== false) {
					echo 'class="active"';
				}
				?>><a href="daftar_pengumuman.php"><i class="fa fa-circle-o"></i> Daftar Pengumuman</a></li>
				<li <?php
				$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
				if (strpos($url,'buat_pengumuman') !== false) {
					echo 'class="active"';
				}
				?>><a href="buat_pengumuman.php"><i class="fa fa-circle-o"></i> Buat Pengumuman</a></li>
			  </ul>
		</li>
		<li class="<?php if(strpos($_SERVER['REQUEST_URI'],'agenda') !== false){echo 'active ';}?>treeview">
          <a href="#">
            <i class="fa fa-calendar"></i> <span>Agenda</span>
            <small class="label pull-right bg-red">3</small>
          </a>
		  <ul class="treeview-menu">
            <li <?php if(strpos($_SERVER['REQUEST_URI'],'daftar_agenda') !== false){echo 'class="active"';}?>><a href="daftar_agenda.php"><i class="fa fa-circle-o"></i> Daftar Agenda</a></li>
            <li <?php if(strpos($_SERVER['REQUEST_URI'],'buat_agenda') !== false){echo 'class="active"';}?>><a href="buat_agenda.php"><i class="fa fa-circle-o"></i> Buat Agenda</a></li>
          </ul>
        </li>
		<li class="<?php if(strpos($_SERVER['REQUEST_URI'],'foto') !== false){echo 'active ';}?>treeview">
			<a href="#"><i class="fa fa-file-photo-o"></i> <span>Foto</span><i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li <?php
				$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
				if (strpos($url,'daftar_foto') !== false) {
					echo 'class="active"';
				}
				?>><a href="daftar_berita.php"><i class="fa fa-circle-o"></i> Daftar Foto</a></li>
				<li <?php
				$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
				if (strpos($url,'upload_album') !== false) {
					echo 'class="active"';
				}
				?>><a href="buat_berita.php"><i class="fa fa-circle-o"></i> Upload Album</a></li>
			</ul>
		</li>
		<li <?php if(strpos($_SERVER['REQUEST_URI'],'user') !== false){echo 'class="active"';}?>><a href="daftar_user.php"><i class="fa fa-user"></i> <span>User</span></a></li>
		<li <?php if(strpos($_SERVER['REQUEST_URI'],'tags') !== false){echo 'class="active"';}?>><a href="daftar_tags.php"><i class="fa fa-tags"></i> <span>Tags</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
