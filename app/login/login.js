'use strict';

angular.module('myApp.login', [])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('login', {
    url: '/login',
    data: {
        permissions: {
          except: ['isloggedin'],
          redirectTo: 'login'
        }
      },
    views: {
      'Content': {
        templateUrl: "login/login.html",
    	  controller: 'LoginCtrl as login'
      }
    }
  })
}])

.controller('LoginCtrl', ['$auth', '$state', '$http', '$rootScope', function($auth, $state, $http, $rootScope) {

      var vm = this;

        vm.loginError = false;
        vm.loginErrorText;
        vm.username;
        vm.password;

        vm.login = function() {

            var credentials = {
                username: vm.username,
                password: vm.password
            }

            $auth.login(credentials).then(function() {
                // Return an $http request for the authenticated user
                $http.get('http://localhost:8000/api/v1/authenticate/user').success(function(response){
                    // Stringify the retured data
                    var user = JSON.stringify(response.user);

                    // Set the stringified user data into local storage
                    localStorage.setItem('user', user);

                    // Getting current user data from local storage
                    $rootScope.currentUser = response.user;
                    $state.go('biodata');

                })
                .error(function(){
                    vm.loginError = true;
                    vm.loginErrorText = error.data.error;
                    console.log(vm.loginErrorText);
                })
            });
        }

}]);
