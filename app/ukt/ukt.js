
angular.module('myApp.ukt', [])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('ukt', {
    url: '/ukt',
    data: {
        permissions: {
          except: ['isloggedin'],
          redirectTo: 'login'
        }
      },
    views: {
      'Content': {
        templateUrl: "ukt/ukt.html",
    	  controller: 'uktCTRL as ukt'
      }
    }
  })
}])

.controller('uktCTRL', ['$http', '$auth', '$rootScope','$state', '$q' , function($http, $auth, $rootScope, $state, $q) {
  var vm = this;

  vm.ukt = function() {
    $http({
      url: 'http://localhost:8000/api/v1/programstudi',
      method: "GET",
    }).success(function(prodi, status, headers, config) {
      vm.change=function(a){
          if (a==1) {
            vm.prodi=prodi.data_bahasa;
          }
          if (a==2) {
            vm.prodi=prodi.data_mipa;
          }
          if (a==3) {
            vm.prodi=prodi.data_teknik;
          }
          if (a==4) {
            vm.prodi=prodi.data_sosial;
          }
          if (a==5) {
            vm.prodi=prodi.data_ekonomi;
          }
          if (a==6) {
            vm.prodi=prodi.data_pendidikan;
          }
          if (a==7) {
            vm.prodi=prodi.data_olahraga;
          }
        }
    });
    $http({
      url: 'http://localhost:8000/api/v1/informasiUKT',
      method: "GET",
    }).success(function(ukt, status, headers, config) {
      vm.change2=function(e){
          if (e==1125) {
            vm.ukt=ukt.data_informasiUKT1;
          }
          else if (e==1215) {
            vm.ukt=ukt.data_informasiUKT2;
          }
          else if (e==1335) {
            vm.ukt=ukt.data_informasiUKT3;
          }
          else if (e==1445) {
            vm.ukt=ukt.data_informasiUKT4;
          }
          else if (e==1515) {
            vm.ukt=ukt.data_informasiUKT5;
          }
          else if (e==1615) {
            vm.ukt=ukt.data_informasiUKT6;
          }
          else if (e==1715) {
            vm.ukt=ukt.data_informasiUKT7;
          }
          else if (e==1815) {
            vm.ukt=ukt.data_informasiUKT8;
          }
          else if (e==2115) {
            vm.ukt=ukt.data_informasiUKT9;
          }
          else if (e==2125) {
            vm.ukt=ukt.data_informasiUKT10;
          }
          else if (e==2215) {
            vm.ukt=ukt.data_informasiUKT11;
          }
          else if (e==2225) {
            vm.ukt=ukt.data_informasiUKT12;
          }
          else if (e==2315) {
            vm.ukt=ukt.data_informasiUKT13;
          }
          else if (e==2415) {
            vm.ukt=ukt.data_informasiUKT14;
          }
          else if (e==2525) {
            vm.ukt=ukt.data_informasiUKT15;
          }
          else if (e==2615) {
            vm.ukt=ukt.data_informasiUKT16;
          }
          else if (e==2715) {
            vm.ukt=ukt.data_informasiUKT17;
          }
          else if (e==2815) {
            vm.ukt=ukt.data_informasiUKT18;
          }
          else if (e==2915) {
            vm.ukt=ukt.data_informasiUKT19;
          }
          else if (e==2925) {
            vm.ukt=ukt.data_informasiUKT20;
          }
          else if (e==3115) {
            vm.ukt=ukt.data_informasiUKT21;
          }
          else if (e==3125) {
            vm.ukt=ukt.data_informasiUKT22;
          }
          else if (e==3135) {
            vm.ukt=ukt.data_informasiUKT23;
          }
          else if (e==3215) {
            vm.ukt=ukt.data_informasiUKT24;
          }
          else if (e==3225) {
            vm.ukt=ukt.data_informasiUKT25;
          }
          else if (e==3315) {
            vm.ukt=ukt.data_informasiUKT26;
          }
          else if (e==3325) {
            vm.ukt=ukt.data_informasiUKT27;
          }
          else if (e==3415) {
            vm.ukt=ukt.data_informasiUKT28;
          }
          else if (e==3425) {
            vm.ukt=ukt.data_informasiUKT29;
          }
          else if (e==4115) {
            vm.ukt=ukt.data_informasiUKT30;
          }
          else if (e==4123) {
            vm.ukt=ukt.data_informasiUKT31;
          }
          else if (e==4315) {
            vm.ukt=ukt.data_informasiUKT32;
          }
          else if (e==4415) {
            vm.ukt=ukt.data_informasiUKT33;
          }
          else if (e==4423) {
            vm.ukt=ukt.data_informasiUKT34;
          }
          else if (e==4715) {
            vm.ukt=ukt.data_informasiUKT35;
          }
          else if (e==4815) {
            vm.ukt=ukt.data_informasiUKT36;
          }
          else if (e==4825) {
            vm.ukt=ukt.data_informasiUKT37;
          }
          else if (e==4915) {
            vm.ukt=ukt.data_informasiUKT38;
          }
          else if (e==5115) {
            vm.ukt=ukt.data_informasiUKT39;
          }
          else if (e==5215) {
            vm.ukt=ukt.data_informasiUKT40;
          }
          else if (e==5223) {
            vm.ukt=ukt.data_informasiUKT41;
          }
          else if (e==5315) {
            vm.ukt=ukt.data_informasiUKT42;
          }
          else if (e==5345) {
            vm.ukt=ukt.data_informasiUKT43;
          }
          else if (e==5353) {
            vm.ukt=ukt.data_informasiUKT44;
          }
          else if (e==5415) {
            vm.ukt=ukt.data_informasiUKT45;
          }
          else if (e==5423) {
            vm.ukt=ukt.data_informasiUKT46;
          }
          else if (e==5433) {
            vm.ukt=ukt.data_informasiUKT47;
          }
          else if (e==5515) {
            vm.ukt=ukt.data_informasiUKT48;
          }
          else if (e==5525) {
            vm.ukt=ukt.data_informasiUKT49;
          }
          else if (e==5535) {
            vm.ukt=ukt.data_informasiUKT50;
          }
          else if (e==5545) {
            vm.ukt=ukt.data_informasiUKT51;
          }
          else if (e==5573) {
            vm.ukt=ukt.data_informasiUKT52;
          }
          else if (e==5583) {
            vm.ukt=ukt.data_informasiUKT53;
          }
          else if (e==5593) {
            vm.ukt=ukt.data_informasiUKT54;
          }
          else if (e==6135) {
            vm.ukt=ukt.data_informasiUKT55;
          }
          else if (e==6215) {
            vm.ukt=ukt.data_informasiUKT56;
          }
          else if (e==6315) {
            vm.ukt=ukt.data_informasiUKT57;
          }
          else if (e==6815) {
            vm.ukt=ukt.data_informasiUKT58;
          }
          else if (e==6825) {
            vm.ukt=ukt.data_informasiUKT59;
          }
          else if (e==8105) {
            vm.ukt=ukt.data_informasiUKT60;
          }
          else if (e==8135) {
            vm.ukt=ukt.data_informasiUKT61;
          }
          else if (e==8143) {
            vm.ukt=ukt.data_informasiUKT62;
          }
          else if (e==8215) {
            vm.ukt=ukt.data_informasiUKT63;
          }
          else if (e==8223) {
            vm.ukt=ukt.data_informasiUKT64;
          }
          else if (e==8323) {
            vm.ukt=ukt.data_informasiUKT65;
          }
          else if (e==8335) {
            vm.ukt=ukt.data_informasiUKT66;
          }
          else {
            return "tidak ada data";
          }
          console.log(vm.ukt);
        }
    });
  };
  vm.ukt();
}]);
