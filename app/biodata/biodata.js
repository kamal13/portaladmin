'use strict';
angular.module('myApp.biodata', [])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('biodata', {
    url: '/biodata',
    data: {
        permissions: {
          except: ['anonymous'],
          redirectTo: 'login'
        }
      },
    views: {
      'Content': {
        templateUrl: "biodata/biodata.html",
    	  controller: 'biodataCTRL as biodata'
      }
    }
  })
}])

.controller('biodataCTRL', ['$http', '$auth', '$rootScope','$state', '$q' , function($http, $auth, $rootScope, $state, $q) {
  var vm = this;

  vm.biodata = function() {
    $http({
      url: 'http://localhost:8000/api/v1/agama',
      method: "GET",
    }).success(function(agama, status, headers, config) {
      vm.agama = agama.data_agama;
    });
    $http({
      url: 'http://localhost:8000/api/v1/propinsi',
      method: "GET",
    }).success(function(propinsi, status, headers, config) {
      vm.propinsi = propinsi.data_propinsi;
    });
    $http({
      url: 'http://localhost:8000/api/v1/kabupaten',
      method: "GET",
    }).success(function(kabupaten, status, headers, config) {
      vm.change5=function(z){
      if (z==1) {
        vm.kabupaten = kabupaten.data_kabupaten1;
      }
      else if (z==10) {
        vm.kabupaten = kabupaten.data_kabupaten10;
      }
      else if (z==11) {
        vm.kabupaten = kabupaten.data_kabupaten11;
      }
      else if (z==12) {
        vm.kabupaten = kabupaten.data_kabupaten12;
      }
      else if (z==13) {
        vm.kabupaten = kabupaten.data_kabupaten13;
      }
      else if (z==14) {
        vm.kabupaten = kabupaten.data_kabupaten14;
      }
      else if (z==15) {
        vm.kabupaten = kabupaten.data_kabupaten15;
      }
      else if (z==16) {
        vm.kabupaten = kabupaten.data_kabupaten16;
      }
      else if (z==17) {
        vm.kabupaten = kabupaten.data_kabupaten17;
      }
      else if (z==18) {
        vm.kabupaten = kabupaten.data_kabupaten18;
      }
      else if (z==19) {
        vm.kabupaten = kabupaten.data_kabupaten19;
      }
      else if (z==2) {
        vm.kabupaten = kabupaten.data_kabupaten2;
      }
      else if (z==20) {
        vm.kabupaten = kabupaten.data_kabupaten20;
      }
      else if (z==21) {
        vm.kabupaten = kabupaten.data_kabupaten21;
      }
      else if (z==22) {
        vm.kabupaten = kabupaten.data_kabupaten22;
      }
      else if (z==23) {
        vm.kabupaten = kabupaten.data_kabupaten23;
      }
      else if (z==24) {
        vm.kabupaten = kabupaten.data_kabupaten24;
      }
      else if (z==25) {
        vm.kabupaten = kabupaten.data_kabupaten25;
      }
      else if (z==26) {
        vm.kabupaten = kabupaten.data_kabupaten26;
      }
      else if (z==27) {
        vm.kabupaten = kabupaten.data_kabupaten27;
      }
      else if (z==28) {
        vm.kabupaten = kabupaten.data_kabupaten28;
      }
      else if (z==29) {
        vm.kabupaten = kabupaten.data_kabupaten29;
      }
      else if (z==3) {
        vm.kabupaten = kabupaten.data_kabupaten3;
      }
      else if (z==30) {
        vm.kabupaten = kabupaten.data_kabupaten30;
      }
      else if (z==31) {
        vm.kabupaten = kabupaten.data_kabupaten31;
      }
      else if (z==32) {
        vm.kabupaten = kabupaten.data_kabupaten32;
      }
      else if (z==33) {
        vm.kabupaten = kabupaten.data_kabupaten33;
      }
      else if (z==34) {
        vm.kabupaten = kabupaten.data_kabupaten34;
      }
      else if (z==4) {
        vm.kabupaten = kabupaten.data_kabupaten4;
      }
      else if (z==5) {
        vm.kabupaten = kabupaten.data_kabupaten5;
      }
      else if (z==6) {
        vm.kabupaten = kabupaten.data_kabupaten6;
      }
      else if (z==7) {
        vm.kabupaten = kabupaten.data_kabupaten7;
      }
      else if (z==8) {
        vm.kabupaten = kabupaten.data_kabupaten8;
      }
      else if (z==9) {
        vm.kabupaten = kabupaten.data_kabupaten9;
      }
      else {
        return "data tidak ditemukan";
      }
      console.log(vm.kabupaten);
      }
    });
    $http({
      url: 'http://localhost:8000/api/v1/penghasilan',
      method: "GET",
    }).success(function(penghasilan, status, headers, config) {
      vm.penghasilan = penghasilan.data_penghasilan;
    });
    $http({
      url: 'http://localhost:8000/api/v1/pendidikan',
      method: "GET",
    }).success(function(pendidikan, status, headers, config) {
      vm.pendidikan = pendidikan.data_pendidikan;
    });
  };
  vm.updatebiodata = function(){
    vm.nama;
    vm.jenis_identitas;
    vm.jenis_kelamin;
    vm.tempat_lahir;
    vm.tanggal_lahir;
    vm.alamat;
    vm.propinsi;
    vm.kabupaten;
    vm.religi;
    vm.nama_ayah;
    vm.nama_ibu;
    vm.pekerjaanIbu;
    vm.pekerjaanAyah;
    vm.penghasilan_ibu;
    vm.penghasilan_ayah;
    vm.jumlah_tanggungan;
    vm.nama_sekolah;
    vm.jenisPendidikan;
    vm.npsn;
    vm.jurusan;
    vm.alamat_sekolah;
    vm.pros;
    vm.kabsek;

    $http.put('http://localhost:8000/api/v1/calonmahasiswa/' + $rootScope.currentUser.id, {
      nama : vm.nama,
      jenisIdentitas : vm.jenis_identitas,
      jenisKelamin : vm.jenis_kelamin,
      tempatlahir : vm.tempat_lahir,
      tanggallahir : vm.tanggal_lahir,
      Alamat : vm.alamat,
      propinsi : vm.propinsi,
      kabupaten : vm.kabupaten,
      agama : vm.religi,
      Ayah : vm.nama_ayah,
      Ibu : vm.nama_ibu,
      kerjaIbu : vm.pekerjaanIbu,
      kerjaAyah : vm.pekerjaanAyah,
      hasilIbu : vm.penghasilan_ibu,
      hasilAyah : vm.penghasilan_ayah,
      Tanggungan : vm.jumlah_tanggungan,
      Nama_sekolah : vm.nama_sekolah,
      jenisAsalSekolah : vm.jenisPendidikan,
      Npsn : vm.npsn,
      jurusanSekolah : vm.jurusan,
      alamatAsalPendidikan : vm.alamat_sekolah,
      propinsiAsalPendidikan : vm.pros,
      kabupatenAsalPendidikan : vm.kabsek
      }).success(function(response) {
        $state.go('berita');
      });
  };
  vm.biodata();
}]);
