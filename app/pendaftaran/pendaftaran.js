'use strict';

angular.module('myApp.pendaftaran', [])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('pendaftaran', {
    url: '/pendaftaran',
    data: {
        permissions: {
          except: ['isloggedin'],
          redirectTo: 'login'
        }
      },
    views: {
      'Content': {
        templateUrl: "pendaftaran/pendaftaran.html",
    	  controller: 'PendaftaranCtrl as pendaftaran'
      }
    }
  })
}])

.controller('PendaftaranCtrl', ['$http', '$auth', '$rootScope','$state', '$q' , function($http, $auth, $rootScope, $state, $q) {
  var vm = this;

  vm.pendaftaran = [];

  vm.error;


  vm.addUser = function() {

    vm.username;
    vm.password;
    vm.confirm_password;
    vm.nama;
    vm.email;
    vm.telepon;
    vm.mode = '5';
    vm.aktif = '1';

      $http.post('http://localhost:8000/api/v1/pendaftaran', {
          username: vm.username,
          password: vm.password,
          nama_lengkap: vm.nama,
          email: vm.email,
          telepon: vm.telepon,
          mode: vm.mode,
          aktif: vm.aktif
      }).success(function(response) {
          vm.password = '';
          vm.confirm_password = '';
          vm.email = '';
          vm.telepon = '';
      }).error(function(){
        console.log("error");
      });

      $http.post('http://localhost:8000/api/v1/calonmahasiswa', {
          nama: vm.nama,
      }).success(function(response) {
          vm.nama = '';
      }).error(function(){
        console.log("error");
      });

      alert("Username Created Successfully");
      $state.go('login');
  };

}]);
