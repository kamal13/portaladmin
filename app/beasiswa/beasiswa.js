
angular.module('myApp.beasiswa', [])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('beasiswa', {
    url: '/beasiswa',
    data: {
        permissions: {
          except: ['isloggedin'],
          redirectTo: 'login'
        }
      },
    views: {
      'Content': {
        templateUrl: "beasiswa/beasiswa.html",
    	  controller: 'beasiswaCTRL as beasiswa'
      }
    }
  })
}])

.controller('beasiswaCTRL', ['$http', '$auth', '$rootScope','$state', '$q' , function($http, $auth, $rootScope, $state, $q) {
  var vm = this;

  vm.beasiswa = [];

  vm.beasiswa = function() {
    $http({
      url: 'http://localhost:8000/api/v1/beasiswa',
      method: "GET",
    }).success(function(beasiswa, status, headers, config) {
      vm.beasiswa = beasiswa.data_beasiswa;
    });
  };
  vm.beasiswa();
}]);
