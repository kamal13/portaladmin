
angular.module('myApp.berita', [])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('berita', {
    url: '/berita',
    data: {
        permissions: {
          except: ['isloggedin'],
          redirectTo: 'login'
        }
      },
    views: {
      'Content': {
        templateUrl: "berita/berita.html",
    	  controller: 'beritaCTRL as berita'
      }
    }
  })
}])

.controller('beritaCTRL', ['$http', '$auth', '$rootScope','$state', '$q' , function($http, $auth, $rootScope, $state, $q) {
  var vm = this;

  vm.berita = [];

  vm.berita = function() {
    $http({
      url: 'http://localhost:8000/api/v1/berita',
      method: "GET",
    }).success(function(berita, status, headers, config) {
      vm.berita = berita.data_berita;
    });
  };
  vm.berita();
}]);
